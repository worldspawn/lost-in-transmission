﻿using UnityEngine;
using System.Collections;

public class FPSDisplay : MonoBehaviour {

	int frames = 0;
	string msg = "";
	
	// Update is called once per frame
	void Update () {
		frames++;
	}

	void Start () {
		InvokeRepeating("EverySecond", 1f, 1f);
	}

	void EverySecond() {
		msg = "FPS: " + frames.ToString();
		frames = 0;
	}

	void OnGUI () {
		GUILayout.Label(msg);
	}

}

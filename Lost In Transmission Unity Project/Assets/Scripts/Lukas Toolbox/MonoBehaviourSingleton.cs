﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A singleton meant to be the only MonoBehaviour of this type in the scene.
/// </summary>

public class MonoBehaviourSingleton<T> : MonoBehaviour where T : MonoBehaviour {

	private static T m_instance;
	public static T Instance {
		get {
			if (m_instance == null) {
				// find in scene
				T[] instances = FindObjectsOfType<T>();
				if (instances.Length > 1) Debug.LogError("There is more than one instance of Singleton '" + typeof(T).ToString() + "' in the scene.");
				if (instances.Length > 0) return instances[0];
				else { // no instances
					string newName = typeof(T).ToString();
					GameObject obj = new GameObject(newName);
					m_instance = obj.AddComponent<T>();
					return m_instance;
				}

			}
			else return m_instance;
		}
	}
}

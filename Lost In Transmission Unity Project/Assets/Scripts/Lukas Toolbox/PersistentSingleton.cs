﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

/// <summary>
/// GameObjects with this script on them will persist throughout scenes, and there will only be one of their kind.
/// Important: Gameobject name has to be unique
/// </summary>

public class PersistentSingleton : MonoBehaviour {

	bool isFirst = false;
	public bool IsFirst {get{return isFirst;}}

	void Awake () {

		// if an instance of me has existed before, destroy me
		PersistentSingleton[] allSingletons = GameObject.FindObjectsOfType<PersistentSingleton>();
		foreach (PersistentSingleton other in allSingletons) { // traverse all other persistentsingletons
			if (other.gameObject.name == gameObject.name) { // if it has the same gameobject name
				if (other.isFirst) { // if another singleton was here first
					Destroy(gameObject); // oh no, I'm doomed!
				}
				else { // if I'm the first and only one
					DontDestroyOnLoad(gameObject); // persist!
				}
			}
		}
	}

	void Start () {
		isFirst = true;
	}
}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// A timer script that calls a delegate
/// </summary>

public class Timer : MonoBehaviour {

	// method to call when time is up
	public delegate void CallBackMethod();
	public CallBackMethod OnTimeUp;

	// is the time runing?
	bool timeIsRunning = false;
	public bool TimeIsRunning {
		get {
			return timeIsRunning;
		}
	}

	// how long to wait
	[SerializeField]
	float intervalTime;
	public float IntervalTime {
		get {
			return intervalTime;
		}
		set {
			intervalTime = value;
		}
	}
	// time that's left
	float timeLeft = 0;
	public float TimeLeft {
		get {
			return timeLeft;
		}
	}
	
	// will the timer repeat?
	[SerializeField]
	bool isRepeating = true;
	public bool IsRepeating {
		get {
			return isRepeating;
		}
		set {
			isRepeating = value;
		}
	}

	// destroy when time is up?
	bool destroyOnTimeUp = false;
	public bool DestroyOnTimeUp {
		get {
			return destroyOnTimeUp;
		}
		set {
			destroyOnTimeUp = value;
		}
	}

	// tell if there is time left
	public bool HasTimeLeft {
		get {
			return (timeLeft > 0f);
		}
	}

	// static factory method to create a timer
	public static Timer Create(float intervalTime = 1f, Transform owner = null, bool destroyOnTimeUp = false) {
		// create gameobj for the timer to be on and create timer
		GameObject obj = new GameObject();
		Timer newTimer = obj.AddComponent<Timer>();

		// set properties
		newTimer.IntervalTime = intervalTime;
		newTimer.DestroyOnTimeUp = destroyOnTimeUp;

		// set parent
		if (owner != null) obj.transform.parent = owner;

		return newTimer;
	}

	// Use this for initialization
	public void StartTimer (CallBackMethod callOnTimeOut = null) {
		OnTimeUp = callOnTimeOut;
		timeLeft = intervalTime;
		timeIsRunning = true;
	}

	// update time running
	void Update() {
		if (timeIsRunning) {
			timeLeft -= Time.deltaTime;
			if (timeLeft <= 0f) {	// if the time is up		

				if (OnTimeUp != null) OnTimeUp(); // call callback methopd if assigned
				if (isRepeating) {
					timeLeft = intervalTime;
				}
				else {
					timeIsRunning = false;
					timeLeft = 0;
					if (destroyOnTimeUp) Destroy(this.gameObject);
				}
			}
		}
	}

	public void Stop() {
		timeIsRunning = false;
		timeLeft = intervalTime;
	}
	public void Pause() {
		timeIsRunning = false;
	}
	public void Play() {
		timeIsRunning = true;
	}

}

﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Listens to and forwards trigger events to other scripts
/// </summary>

public class TriggerEventListener : MonoBehaviour {

	public delegate void TriggerEvent(Collider collider);
	public TriggerEvent triggerEvent;

	void OnTriggerEnter(Collider collider) {
		if (triggerEvent != null) triggerEvent(collider);
	}
}

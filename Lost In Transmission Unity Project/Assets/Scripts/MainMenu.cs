﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour {

    private Button startButton;
    private Button exitButton;

    void Start()
    {

        Debug.Log("XXX");

        startButton = GameObject.Find("ButtonStartGame").GetComponent<Button>();
        exitButton = GameObject.Find("ButtonExitGame").GetComponent<Button>();

        startButton.onClick.AddListener(StartGame);
        exitButton.onClick.AddListener(ExitGame);

    }

    void StartGame()
    {
        Debug.Log("Starting the first scenario!");
    }

    void ExitGame()
    {
        Debug.Log("Closing the game.");
        Application.Quit();
    }
}

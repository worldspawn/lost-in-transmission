﻿using Cradle;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ScenarioManager : MonoBehaviourSingleton<ScenarioManager>
{
    public Image backgroundImage;
    public Image characterImage;
    public Image fadeInImage;
    public float fadeInTime = 3f;
    private float fadeInTimeLeft;
    public Transform contentPanel;
    public GameObject gameOvertext;
    private Scenario currentScenario;
    private Scenario.Character currentCharacter;
    private StoryState storyState;
    private bool lastScreen = false;
    public AudioSource musicAudioSource;
    public AudioSource sfxAudioSource;

    void Start()
    {
        Invoke("StartScenario", 0f);
        fadeInTimeLeft = fadeInTime;
    }

    void StartScenario ()
    {
        //Debug.Log("Starting Scenario");
        gameOvertext.SetActive(false);

        Scenario scenarioLink = FindObjectOfType<CurrentScenario>().currentScenario;
        GameObject obj = Instantiate(scenarioLink.gameObject);
        currentScenario = obj.GetComponent<Scenario>();

        FindObjectOfType<OurOwnTwineTextPlayer>().StartTheStory(currentScenario);
    }

    public void SwitchScreen(string screenName)
    {
        Debug.Log("Switching screen to " + screenName);
        //musicAudioSource.Stop();
        foreach(Scenario.Screen screen in currentScenario.screens)
        {
            if (screen.name == screenName)
            {
                backgroundImage.sprite = screen.background;
                SetCurrentCharacter(screen.character);
                return;
            }
        }

        Debug.LogError("Screen does not exist: " + screenName);
    }

    public void ChangeMood(string moodName)
    {
        Debug.Log("Changing mood to " + moodName);

        foreach (Scenario.Character.Emotion emotion in currentCharacter.images)
        {
            if (emotion.name == moodName)
            {
                characterImage.sprite = emotion.image;
                return;
            }
        }

        Debug.LogError("Mood does not exist: " + moodName);
    }

    public void PlayMusic(string fileName)
    {
        if (fileName == "")
        {
            musicAudioSource.Stop();
            return;
        }

        Object[] objs = Resources.LoadAll("", typeof(AudioClip));
        foreach (Object obj in objs)
        {
            AudioClip clip = obj as AudioClip;
            //Debug.Log(clip.name);
            if (clip != null && clip.name.ToUpper() == fileName.ToUpper())
            {
                musicAudioSource.Stop();
                musicAudioSource.clip = clip;
                musicAudioSource.Play();
                return;
            }
        }

        Debug.LogError("Music file does not exist: " + fileName);
    }

    public void PlaySoundEffect(string fileName)
    {
        Object[] objs = Resources.LoadAll("", typeof(AudioClip));
        foreach (Object obj in objs)
        {
            AudioClip clip = obj as AudioClip;
            //Debug.Log(clip.name);
            if (clip != null && clip.name.ToUpper() == fileName.ToUpper())
            {
                sfxAudioSource.PlayOneShot(clip);
                return;
            }
        }

        Debug.LogError("Sound effect file does not exist: " + fileName);
    }

    // change the current character
    private void SetCurrentCharacter(string characterName)
    {
        if (string.IsNullOrEmpty(characterName))
        {
            characterImage.enabled = false;
            return;
        }

        ;
        characterImage.enabled = true;

        foreach (Scenario.Character character in currentScenario.characters)
        {
            if (character.name == characterName)
            {
                currentCharacter = character;
                characterImage.sprite = character.images[0].image;
                return;
            }
        }

        Debug.LogError("Character does not exist: " + characterName);
    }

    public void OnStoryStateChanged(StoryState obj)
    {
        Debug.Log("Story State: " + obj);
        
        if (obj == StoryState.Idle)
        {
            int count = 0;
            foreach (TwineTextPlayerElement element in contentPanel.GetComponentsInChildren<TwineTextPlayerElement>())
            {
                if (IsLink(element.name)) count++;
            }
            
            if (count == 0)
            {
                Invoke("InitLastScreen", .7f);
            }
        }        
    }

    private void InitLastScreen ()
    {
        lastScreen = true;
        gameOvertext.SetActive(true);
    }

    private void Update ()
    {
        if (Input.GetKeyDown(KeyCode.Escape)) SceneManager.LoadScene(0);

        if (lastScreen)
        {
            if (Input.anyKey) SceneManager.LoadScene(0);
        }
        if (fadeInTimeLeft > 0f)
        {
            fadeInImage.color = new Color(0f, 0f, 0f, (fadeInTimeLeft / fadeInTime));
            fadeInTimeLeft -= Time.deltaTime;
            if (fadeInTimeLeft < 0f) fadeInImage.enabled = false;
        }
    }

    private bool IsLink(string s)
    {
        return (s.Contains("[[") && s.Contains("]]"));
    }
}

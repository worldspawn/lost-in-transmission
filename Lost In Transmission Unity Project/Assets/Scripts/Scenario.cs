﻿using Cradle;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scenario : MonoBehaviour {    

    public Story GetStory ()
    {
        Story story = GetComponent<Story>();
        if (story == null) Debug.LogError("Scenario " + gameObject.name + " has no story assigned. Please add one to the prefab / gameObject.");
        return story;
    }

    public Screen[] screens;
    public Character[] characters;

    [System.Serializable]
    public class Screen
    {
        public string name;
        public Sprite background;
        public string character;
    }

    [System.Serializable]
    public class Character
    {
        public string name;
        public Emotion[] images;

        [System.Serializable]
        public class Emotion
        {
            public string name;
            public Sprite image;
        }
    }
}

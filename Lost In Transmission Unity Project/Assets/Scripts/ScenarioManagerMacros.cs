﻿using Cradle;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScenarioManagerMacros : Cradle.RuntimeMacros {

    [RuntimeMacro]
    public void SwitchScreen(string screenName)
    {
        ScenarioManager.Instance.SwitchScreen(screenName);
    }

    [RuntimeMacro]
    public void ChangeMood(string moodName)
    {
        ScenarioManager.Instance.ChangeMood(moodName);
    }

    [RuntimeMacro]
    public void PlayMusic(string fileName)
    {
        ScenarioManager.Instance.PlayMusic(fileName);
    }

    [RuntimeMacro]
    public void PlaySoundEffect(string fileName)
    {
        ScenarioManager.Instance.PlaySoundEffect(fileName);
    }
}

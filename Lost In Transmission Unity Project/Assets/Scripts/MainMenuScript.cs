﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class MainMenuScript : MonoBehaviour {

    private Button buttonStartGoneFishing1;
    private Button buttonStartGoneFishing2;
    private Button buttonStartGoneFishing3;
    private Button buttonStartPizzaStory;
    private Button easterEggButton;

    private Button exitButton;

    public List<Scenario> allScenarios;
    private CurrentScenario currentScenario;
    private float timer;
    private bool turnLeft;

    void Start()
    {
        buttonStartGoneFishing1 = GameObject.Find("ButtonGoneFishing1").GetComponent<Button>();
        buttonStartGoneFishing2 = GameObject.Find("ButtonGoneFishing2").GetComponent<Button>();
        buttonStartGoneFishing3 = GameObject.Find("ButtonGoneFishing3").GetComponent<Button>();
        buttonStartPizzaStory = GameObject.Find("ButtonPizzaStory").GetComponent<Button>();
        easterEggButton = GameObject.Find("ButtonEasterEgg").GetComponent<Button>();

        exitButton = GameObject.Find("ButtonExitGame").GetComponent<Button>();

        buttonStartGoneFishing1.onClick.AddListener(StartGoneFishing1);
        buttonStartGoneFishing2.onClick.AddListener(StartGoneFishing2);
        buttonStartGoneFishing3.onClick.AddListener(StartGoneFishing3);
        buttonStartPizzaStory.onClick.AddListener(StartPizzaStory);

        easterEggButton.onClick.AddListener(StartEasterEgg);

        exitButton.onClick.AddListener(ExitGame);

        currentScenario = FindObjectOfType<CurrentScenario>();
        turnLeft = true;
    }

    void StartGoneFishing1()
    {
        Debug.Log("Starting the first scenario!");
        currentScenario.currentScenario = allScenarios[0];
        SceneManager.LoadScene("ScenarioPlayer", LoadSceneMode.Single);

    }
    void StartGoneFishing2()
    {
        Debug.Log("Starting the first scenario!");
        currentScenario.currentScenario = allScenarios[1];
        SceneManager.LoadScene("ScenarioPlayer", LoadSceneMode.Single);

    }
    void StartGoneFishing3()
    {
        Debug.Log("Starting the first scenario!");
        currentScenario.currentScenario = allScenarios[2];
        SceneManager.LoadScene("ScenarioPlayer", LoadSceneMode.Single);

    }
    void StartPizzaStory()
    {
        Debug.Log("Starting the first scenario!");
        currentScenario.currentScenario = allScenarios[3];
        SceneManager.LoadScene("ScenarioPlayer", LoadSceneMode.Single);

    }
    void Update()
    {
        
        timer += Time.deltaTime;
        if (timer % 60 > 1.5f)
        {
            ToggleTurning();
            timer = 0;
        }

        if (turnLeft)
        {
            easterEggButton.transform.Rotate(Vector3.forward * Time.deltaTime * 30, Space.World);
        }
        else
        {
            easterEggButton.transform.Rotate(Vector3.back * Time.deltaTime * 30, Space.World);
        }
    }

    void ExitGame()
    {
        Debug.Log("Closing the game.");
        Application.Quit();
    }

    void ToggleTurning()
    {
        if (turnLeft)
        {
            turnLeft = false;
        }
        else
        {
            turnLeft = true;
        }
    }

    void StartEasterEgg()
    {
        Debug.Log("Starting the first scenario!");
        currentScenario.currentScenario = allScenarios[4];
        SceneManager.LoadScene("ScenarioPlayer", LoadSceneMode.Single);
    }
}
